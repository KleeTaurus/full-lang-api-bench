import json
import falcon
import requests


class JSONResource(object):
    def on_get(self, request, response):
        json_data = {'language': 'python', 'message': 'Hello, world!'}
        response.body = json.dumps(json_data)


app = falcon.API()

app.add_route('/json', JSONResource())
