# export GUNICORN_CMD_ARGS="--bind=0.0.0.0:5000 --workers=2"
export GUNICORN_CMD_ARGS="--bind=0.0.0.0:5000 --workers=4 --worker-class=gevent"
# export GUNICORN_CMD_ARGS="--bind=0.0.0.0:5000 --workers=2 --worker-class=meinheld.gmeinheld.MeinheldWorker"
gunicorn app:app
