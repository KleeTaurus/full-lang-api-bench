falcon==1.3.0
gunicorn==19.7.1
meinheld==0.6.1
requests==2.18.4
gevent==1.2.2
